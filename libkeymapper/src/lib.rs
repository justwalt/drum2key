use serde::de::{Deserializer, MapAccess, Visitor};
use serde::ser::{SerializeMap, Serializer};
use serde::{Deserialize, Serialize};

use std::fmt;

use std::fs::File;
use std::io::prelude::*;

use std::collections::HashMap;

#[derive(Default, Clone)]
pub struct Keymap {
    pub mappings: HashMap<u8, Binding>,
}

#[derive(Default, Clone, Serialize, Deserialize, Hash)]
pub struct Binding {
    pub channel: u8,
    pub keybind: char,
    pub velocity: u8,
}

impl Keymap {
    pub fn new() -> Keymap {
        Keymap {
            mappings: HashMap::new(),
        }
    }

    pub fn create_new_binding(&mut self, note: u8, binding: Binding) {
        self.mappings.insert(note, binding);
    }

    pub fn get_binding(&self, note: u8) -> Option<&Binding> {
        self.mappings.get(&note)
    }

    pub fn new_from_config_file(filename: &str) -> Keymap {
        let mut toml_string = String::new();

        let mut file = match File::open(&filename) {
            Ok(file) => file,
            Err(_) => {
                println!("The keymap file couldn't be found at {}!", filename);
                std::process::exit(1)
            }
        };

        file.read_to_string(&mut toml_string)
            .unwrap_or_else(|err| panic!("Error while reading TOML: [{}]", err));

        toml::from_str(&toml_string).unwrap()
    }

    pub fn save_config_file(&self, filename: &str) {
        let toml_string = toml::to_string(&self).unwrap();

        let mut file = match File::create(&filename) {
            Ok(file) => file,
            Err(_) => {
                println!("Could not write to the file at {}!", filename);
                std::process::exit(1)
            }
        };

        file.write_all(toml_string.as_bytes())
            .expect("Couldn't write to file!");
    }
}

impl Serialize for Keymap {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_map(Some(self.mappings.len()))?;
        for (k, v) in &self.mappings {
            seq.serialize_entry(&k.to_string(), &v)?;
        }
        seq.end()
    }
}

impl<'de> Visitor<'de> for Keymap {
    // The type that our Visitor is going to produce.
    type Value = Keymap;

    // Format a message stating what data this Visitor expects to receive.
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("keybind map")
    }

    // Deserialize MyMap from an abstract "map" provided by the
    // Deserializer. The MapAccess input is a callback provided by
    // the Deserializer to let us see each entry in the map.
    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        let mut map = Keymap::new();

        // While there are entries remaining in the input, add them
        // into our map.
        while let Some((key, value)) = access.next_entry()? {
            let midi_note_str: String = key;
            let midi_note: u8 = midi_note_str
                .parse::<u8>()
                .expect("Midi note value in map is outside 0-255.");
            map.create_new_binding(midi_note, value);
        }

        Ok(map)
    }
}

// This is the trait that informs Serde how to deserialize MyMap.
impl<'de> Deserialize<'de> for Keymap {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        // Instantiate our Visitor and ask the Deserializer to drive
        // it over the input data, resulting in an instance of MyMap.
        deserializer.deserialize_map(Keymap::new())
    }
}

impl Binding {
    pub fn new(channel: u8, keybind: char, velocity: u8) -> Binding {
        Binding {
            channel,
            keybind,
            velocity,
        }
    }
}
