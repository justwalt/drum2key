// use std::path::Path;
use dirs::home_dir;
use std::fs;
use std::path::PathBuf;
use structopt::StructOpt;

use std::{thread, time};

// use enigo::*;
use midir::{Ignore, MidiInput, MidiInputConnection};
use rdev::{simulate, EventType, Key}; //SimulateError

use libargparser::Args;
use libkeymapper::{Binding, Keymap};

// TODO:
// 1. reconnect if the kit isn't available
// 2. formalize the suppress feature
// X. Add default config location for Windows

fn main() {
    let input_args = Args::from_args();
    let config: PathBuf = match input_args.input {
        Some(path) => path,
        // None => PathBuf::from("./keymap.toml"),
        None => platform_specific_default_config_loc(),
    };

    // Check if config file exists, load it if it does, exit if it doesn't.
    if config.exists() {
        // load it using the method from libtoml in the stitcher
        println!(
            "Loaded config file from {}.",
            config.to_str().expect("Couldn't parse filename.")
        );
    } else {
        regenerate_config_file(config.to_str().expect("Couldn't parse filename."));
    }

    let keymap = Keymap::new_from_config_file(config.to_str().expect("Couldn't parse filename."));

    let midi_connections = make_midi_connections(keymap, input_args.verbose, input_args.suppress);

    runloop(midi_connections);
}

fn runloop(_midi_connections: Vec<MidiInputConnection<()>>) {
    let one_sec = time::Duration::from_secs(1);
    loop {
        thread::sleep(one_sec);
    }
}

fn make_midi_connections(
    keymap: Keymap,
    verbose: bool,
    suppress: bool,
) -> Vec<MidiInputConnection<()>> {
    let midi_devices = MidiInput::new("ports").expect("no creation");
    let midi_ports = midi_devices.ports();

    let mut midi_connections = vec![];

    for port in midi_ports {
        let mut midi_device = MidiInput::new("new input").expect("no creation");
        midi_device.ignore(Ignore::None);

        match midi_device.port_name(&port) {
            Ok(name) => {
                print!("Connecting to {} ... ", name);
            }
            Err(_) => continue,
        }

        let keymap_clone = keymap.clone();
        let midi_connection = midi_device
            .connect(
                &port,
                "keywatcher",
                move |_timestamp, midi_msg, _| {
                    process_midi_message(midi_msg, &keymap_clone, verbose, suppress);
                },
                (),
            )
            .expect("couldn't connect");

        println!("connected.");

        midi_connections.push(midi_connection);
    }

    midi_connections
}

fn process_midi_message(midi_msg: &[u8], keymap: &Keymap, verbose: bool, suppress: bool) {
    // Keymap is a HashMap, so we get the key value from the midi message and then get
    // the rest of the binding info from the keymap.

    // println!("{:?}", midi_msg);

    // pull out the important bits
    let raw_event = midi_msg[0];
    let note = midi_msg[1];
    let velocity = midi_msg[2];

    let channel = raw_event % 16;

    // Note on events:
    if (raw_event < 160_u8) & (raw_event >= 144_u8) {
        // Event values between 144-159 mean a note_on event
        if verbose {
            print!(
                "Midi note value: {:>3}, Channel: {:>2}, Velocity: {:>3} -> ",
                note, channel, velocity
            );
        }
        let binding = match keymap.get_binding(note) {
            Some(bind) => bind,
            None => {
                if verbose {
                    println!(
                        "No binding set for midi note {} (vel={}, ch={}).",
                        note, velocity, channel
                    );
                }
                return;
            }
        };

        if !suppress {
            if channel == binding.channel {
                if velocity >= binding.velocity {
                    // activate key from binding.keybind
                    // TODO: add in keypress from rdev
                    press_key(binding.keybind, verbose);
                    // keypusher.key_click(Key::Layout(binding.keybind));
                    if verbose {
                        println!("pressed key {}", binding.keybind);
                    }
                } else {
                    if verbose {
                        println!("velocity {} < {}", velocity, binding.velocity);
                    }
                }
            } else {
                if verbose {
                    println!("The channel value '{}' doesn't match.", channel);
                }
            }
        }
    };
}

fn press_key(key_char: char, verbose: bool) {
    let key = match key_char {
        'q' => Key::KeyQ,
        'w' => Key::KeyW,
        'e' => Key::KeyE,
        'r' => Key::KeyR,
        't' => Key::KeyT,
        'y' => Key::KeyY,
        'u' => Key::KeyU,
        'i' => Key::KeyI,
        'o' => Key::KeyO,
        'p' => Key::KeyP,
        'a' => Key::KeyA,
        's' => Key::KeyS,
        'd' => Key::KeyD,
        'f' => Key::KeyF,
        'g' => Key::KeyG,
        'h' => Key::KeyH,
        'j' => Key::KeyJ,
        'k' => Key::KeyK,
        'l' => Key::KeyL,
        'z' => Key::KeyZ,
        'x' => Key::KeyX,
        'c' => Key::KeyC,
        'v' => Key::KeyV,
        'b' => Key::KeyB,
        'n' => Key::KeyN,
        'm' => Key::KeyM,
        'N' => Key::Return,
        _ => {
            if verbose {
                println!("Key value not hardcoded, oops");
            };
            return;
        }
    };

    simulate(&EventType::KeyPress(key)).expect("Something slipped through the cracks");
    thread::sleep(time::Duration::from_millis(5));
    simulate(&EventType::KeyRelease(key)).expect("Something slipped through the cracks");
}

fn regenerate_config_file(filepath: &str) {
    // This function will recreate a new map file if it gets lost.
    println!(
        "No keymap found at {}\nCreating a new one. The default note and channel values in this file\ncorrespond to the defaults for a Roland TD-11, and might not match what\nyour drumset outputs.",
        filepath
    );

    let mut keymap = Keymap::new();
    keymap.create_new_binding(37, Binding::new(9, 'z', 30)); // snare rim
    keymap.create_new_binding(38, Binding::new(9, 'z', 30)); // snare head
    keymap.create_new_binding(26, Binding::new(9, 'x', 17)); // hi-hat rim
    keymap.create_new_binding(46, Binding::new(9, 'x', 30)); // hi-hat bell
    keymap.create_new_binding(48, Binding::new(9, 'x', 30)); // tom 1
    keymap.create_new_binding(45, Binding::new(9, 'c', 30)); // tom 2
    keymap.create_new_binding(51, Binding::new(9, 'v', 30)); // crash bell
    keymap.create_new_binding(59, Binding::new(9, 'v', 17)); // crash rim
    keymap.create_new_binding(43, Binding::new(9, 'v', 30)); // tom 3
    keymap.create_new_binding(49, Binding::new(9, 'c', 30)); // ride bell
    keymap.create_new_binding(55, Binding::new(9, 'c', 17)); // ride rim
    keymap.create_new_binding(36, Binding::new(9, 'b', 14)); // kick
    keymap.save_config_file(filepath);
}

#[cfg(unix)]
fn platform_specific_default_config_loc() -> PathBuf {
    let mut config_location: PathBuf = match home_dir() {
        Some(dir) => dir,
        None => return PathBuf::from("./drum2key_keymap.toml"),
    };

    config_location.push(".config");

    if !config_location.exists() {
        match fs::create_dir(config_location.clone().into_os_string()) {
            Ok(_) => (),
            Err(_) => return PathBuf::from("./drum2key_keymap.toml"),
        }
    }

    config_location.push("drum2key_keymap.toml");

    config_location
}

#[cfg(windows)]
fn platform_specific_default_config_loc() -> PathBuf {
    // TODO: add Windows specific default location
    PathBuf::from("./drum2key_keymap.toml")
}
