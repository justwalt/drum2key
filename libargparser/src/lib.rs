use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "Drum2Key",
    about = "Maps midi inputs to keyboard presses, with configuration!",
    version = "0.0.1",
    author = "JAW"
)]
pub struct Args {
    /// Input keymap file. Defaults to keymap.toml if no file is given.
    #[structopt(parse(from_os_str))]
    pub input: Option<PathBuf>,

    /// Verbose mode
    #[structopt(short, long)]
    pub verbose: bool,

    /// Suppress keystrokes
    #[structopt(short, long)]
    pub suppress: bool,
}
