# Drum2Key

### Download

https://gitlab.com/justwalt/drum2key/-/releases

### Running D2K
Just download the executable and run. On Windows, make sure to leave the cmd
window open.

When D2K is run the first time, it creates keymap.toml in the same folder, which
holds default values for my own drum kit. Those might not match yours, so try
customizing the keymap. Note that you could use D2K for keyboard input as well,
but held notes will not work.

### Customize your keymap:
Edit "keymap.toml" which will be generated in the same directory as the drum2key
executable. To help figure out which midi notes to use, run drum2key with the
--verbose option (on Windows, you'll have to use cmd.exe to do this). This will
print midi information every time a midi event is detected. In the future I plan
to streamline this process, but this should do for now.
